#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

const int p = 5;
const int r = 3;
const int k = 2;

struct Triple {
    int a;
    int b;
    int c;

    bool isZero() {
        return a == 0 && b == 0 && c == 0;
    }

    Triple& operator+=(const Triple& rhs) {
        this->a = (this->a + rhs.a) % p;
        this->b = (this->b + rhs.b) % p;
        this->c = (this->c + rhs.c) % p;
        return *this;
    }

    friend Triple operator+(Triple lhs, const Triple& rhs) {
        lhs += rhs;
        return lhs;
    }
};

struct SumTracker {
    Triple sum;
    int used;
};

void fillVector(vector<Triple>& vec);
bool doesZeroSumSubsetExist(vector<Triple>& available);
void printSums(vector<SumTracker>& sums);
void printVector(vector<Triple>& vec);

int main() {
    int numTriples = pow(p, r);

    vector<Triple> vec;
    fillVector(vec);

    long trials = 0;
    //Generate all subsets of 10 vectors
    //The recursive way is much better
    //I hate myself
    //
    for(int a1 = 0; a1 < numTriples; a1++){
        for(int a2 = a1 + 1; a2 < numTriples; a2++) {
            for(int a3 = a2 + 1; a3 < numTriples; a3++) {
                for(int a4 = a3 + 1; a4 < numTriples; a4++) {
                    for(int a5 = a4 + 1; a5 < numTriples; a5++) {
                        for(int a6 = a5 + 1; a6 < numTriples; a6++) {
                            //for(int a7 = a6 + 1; a7 < numTriples; a7++) {
                                //for(int a8 = a7 + 1; a8 < numTriples; a8++) {
                                    //for(int a9 = a8 + 1; a9 < numTriples; a9++) {
                                        //for(int a10 = a9 + 1; a10 < numTriples; a10++) {
                                            trials++;
                                            //Print out progress every million trials
                                            if(trials % 100000 == 0) {
                                                cout << trials << endl;
                                            }
                                            vector<Triple> toTest;
                                            toTest.push_back(vec[a1]);
                                            toTest.push_back(vec[a2]);
                                            toTest.push_back(vec[a3]);
                                            toTest.push_back(vec[a4]);
                                            toTest.push_back(vec[a5]);
                                            toTest.push_back(vec[a6]);
                                         //   toTest.push_back(vec[a7]);
                                          //  toTest.push_back(vec[a8]);
                                           // toTest.push_back(vec[a9]);
                                            //toTest.push_back(vec[a10]);
                                            if(!doesZeroSumSubsetExist(toTest)) {
                                                cout << "I found one that doesn't work:" << endl;
                                                printVector(toTest);
                                                return 0;
                                            //}
                                        //}
                                    //}
                                //}
                            }
                        }
                    }
                }
            }
        }
    }

    cout << "conjecture painstakingly verified!" << endl;
    cout << "total trials: " << trials << endl;

}

void fillVector(vector<Triple>& vec){
    for(int a = 0; a < p; a++) {
        for(int b = 0; b < p; b++) {
            for(int c = 0; c < p; c++) {
                vec.push_back( {a, b, c} );
            }
        }
    }
}

bool doesZeroSumSubsetExist(vector<Triple>& available) {
    int targetLength = k * p;
    vector<SumTracker> sums;
    //For every element in the vector
    vector<Triple>::iterator curVec;
    for(curVec = available.begin(); curVec != available.end(); ++curVec) {
        //Go through all of the current sums, and stop when we hit all the ones
        //that existed before this round
        vector<SumTracker>::iterator curSum;
        size_t numToTest = sums.size();
        size_t curIndex = 0;
        for(curSum = sums.begin(); curIndex < numToTest && curSum != sums.end(); ++curSum) {
            if(curSum->used < targetLength) { //We can build more
                SumTracker newSum = *curSum;
                //Add as many copies of the vector as we're allowed
                for(int i = 0; i < p - 1; i++) {
                    if(newSum.used >= k * p) break;
                    newSum.sum += *curVec;
                    newSum.used += 1;
                    sums.push_back(newSum);

                    //We did it!
                    if(newSum.used == targetLength && newSum.sum.isZero()) return true;
                }
            }
            ++curIndex;
        }

        //Lastly, add a bunch of sums that use only this vector
        //We don't have to worry about getting a ZSSS, since there's only p-1
        //of each vector
        SumTracker newSum;
        newSum.sum = {0, 0, 0};
        newSum.used = 0;
        for(int i = 0; i < p - 1; i++) {
            newSum.sum += *curVec;
            newSum.used += 1;
            sums.push_back(newSum);
        }

        //cout << "I now have " << sums.size() << " sums:" << endl;
        //printSums(sums);
    }

    //If we get here, we've failed
    return false;
}

//For debugging
void printSums(vector<SumTracker>& sums) {
    vector<SumTracker>::iterator curSum;
    for(curSum = sums.begin(); curSum != sums.end(); ++curSum) {
        cout << "{" << curSum->sum.a << ", " << curSum->sum.b << ", " <<
            curSum->sum.c << "}" << " using " << curSum->used << " vectors " << endl;
    }
}

void printVector(vector<Triple>& vec) {
    vector<Triple>::iterator curVec;
    for(curVec = vec.begin(); curVec != vec.end(); ++curVec) {
        cout << "{" << curVec->a << ", " << curVec->b << ", " <<
            curVec->c << "}" << endl;
    }
}
