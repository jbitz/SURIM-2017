/*
 * relations.cc
 * Jared Bitz, August 2017
 * ----------------------------------
 * Computes a relation matrix for congruences on the number of length kp
 * subsequences in a set of a given size. (These congruences ultimately
 * result from Reiher's application of the Chevalley-Warning Theorem and
 * the sum-over-subsets technique). Output is a row-reduced matrix for
 * convenience.
 */

#include <iostream>

using namespace std;

int main(int argc, char* argv[]) {
    if(argc < 2) {
        cout << "Calculates a reduced relation matrix for a sequence" << endl;
        cout << "of size kp - 1 in C_p^3. Please pass your desired" << endl;
        cout << "value of k as an argument when running." << endl;
        return 0;
    }

    int k;
    try{
        k = stoi(argv[1]);
    } catch (...) {
        cout << "Error: argument must be an integer" << endl;
        return 0;
    }
    cout << "Calculating for |X| = " << k << "p - 1." << endl << endl;

    int numRows = k - 3;
    int numCols = k;
    int relationMatrix[numRows][numCols];

    //Construct Pascal's triangle
    for(int row = 0; row < numRows; row++) {
        for(int col = numCols - 1; col >= 0; col--) { //Build from right
            if(row == 0 || row == numCols - 1 - col){ //First row is ones
                relationMatrix[row][col] = 1;
            } else if (col > numCols - row - 1){ //Zeros in the bottom right
                relationMatrix[row][col] = 0;
            } else { //Sum of the 'above' elements otherwise
                relationMatrix[row][col] = abs(relationMatrix[row][col + 1])
                    + abs(relationMatrix[row - 1][col + 1]);
            }
            relationMatrix[row][col] *= col % 2 == 0 ? 1 : -1;
        }
    }

    //Row reduction - easy because the matrix is upper triangular
    for(int row = 0; row < numRows; row++) {
        int pivot = numCols - row - 1; //Location of the 1 to keep
        for(int col = pivot - 1; col > 2; col--) { //Always two free variables
            int curVal = relationMatrix[row][col];
            int multiplier = curVal * (col % 2 == 0 ? 1 : -1);
            for(int i = 0; i < pivot; i++) {
                relationMatrix[row][i] -=  multiplier *
                    relationMatrix[numCols - 1 - col][i];
            }
        }
        //Make the pivot always +1
        if(relationMatrix[row][pivot] == -1) {
            for(int col = 0; col <= pivot; col++)
                relationMatrix[row][col] *= -1;
        }
    }

    //Print matrix
    for(int col = 0; col < numCols; col++) { //Header
        cout << "b_" << col << "\t";
    }
    cout << endl;
    for(int col = 0; col < numCols; col++) { //Separator
        cout << "--------";
    }
    cout << endl;
    for(int row = 0; row < numRows; row++) { //Values
        for(int col = 0; col < numCols; col++) {
            cout << relationMatrix[row][col] << "\t";
        }
        cout << endl;
    }
    cout << endl;
}
