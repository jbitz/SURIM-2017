In 1961, Erd\H{os}, Ginzburg, and Ziv \cite{Erdos1961} published a paper
proving that among any $2n - 1$ integers, one can always find $n$ of them with
an average which is also an integer.
Today, this result is usually phrased more abstractly in terms of abelian groups.
Namely, for any $(2n - 1)$-sequence in $C_n$, there exists a subsequence of
exactly $n$ elements which sums to zero.
There are two directions in which additive number theorists seek to generalize
this result.
The first is in defining the Erd\H{o}s-Ginzburg-Ziv constant
for arbitrary groups.

\begin{definition}
    Let $G$ be a finite abelian group with exponent $p$.
    Then, the
    \textbf{Erd\H{o}s-Ginzburg-Ziv} constant of $G$, denoted $s_p(G)$,
    is the smallest $m$ such that a sequence of $m$ elements in $G$ always
    contains a subsequence of $p$ elements which sums to zero.
\end{definition}

It is important to note that we have defined the EGZ constant for
\textit{subsequences} and not \textit{subsets} - we will allow multiplicity.
It is also easy to see why our zero-sum subsequences must have their lengths
be a multiple of $p$.
If instead we sought sequences of length $k$ for $k \equiv a$ mod $p$,
and $a \neq 0$, we could choose some element $g \in G$ of order $p$.
Then the sequence consisting $g$ repeated an arbitrarily large number of times
has no length $k$ zero-sum subsequence, as $k \cdot g = a \cdot g \neq 0$.
Thus, the EGZ constant $s_k(G)$ would not exist.

Of course, we can still consider sequences whose lengths are $0$ mod $p$, and
this opens the door to defining an even more general property
which is the central focus of this paper.

\begin{definition}
    Let $G$ be a finite abelian group with exponent $p$.
    The \textbf{$k$-th generalized EGZ constant}, denoted $\skp(G)$, is
    the smallest $m$ such that a sequence of $m$ elements in $G$ always
    contains a subsequence of $kp$ elements which sums to zero.
\end{definition}

At this point, it may seem curious that we have been dealing implicitly
with $p$-groups (i.e. groups of prime exponent).
In most cases, and especially in the case of cyclic groups and their powers,
these turn out to be the most interesting structurally, as we can synthesize results
for $p$-groups and $q$-groups into results about $pq$-groups.
In fact, the results we prove in \autoref{sec:background} will
use this exact principle to simplify the calculation of the EGZ constant from
general cyclic groups to that of $p$-groups.

The EGZ problem, especially in the ``smallest'' case of $k = 1$, is much
more intractible than it originally appears.
In the almost sixty years since the Erd\H{o}s paper, the only groups for which
$s_p$ has been exactly determined are cyclic groups $C_n$ and the
two-dimensional case $C_n^2$.
The former result of $s_{n}(C_n) = 2p - 1$ comes from the original Erd\H{o}s
paper \cite{Erdos1961}, while the latter of $s_{n}(C_n^2) = 4p - 3$ is
rather recent, and due to Reiher. \cite{Reiher2007}
Although it was Reiher who pinned down the actual value for $C_n^2$,
R\'onyai also did notable work, proving an upper bound of $4p - 2$.
\cite{Ronyai2000}
Our results especially make use of congruence-based techniques developed by
Reiher, and so a thorough summary of these will follow.

More recently, Naslund has proven exponential upper
bounds on $s_p(C_p^r)$ for general $r$ by using the so-called
``slice rank'' method, which generalizes the standard tensor rank of a function.
\cite{Naslund2017}.
Specifically, he shows that
\begin{equation*}
    s_p(C_p^r) < 3p!(2p-1)(J(p)p)^r,
\end{equation*}
where $J(p)$ is a constant, depending on $p$, which lies between
.918 and .841 and decreases as $p$ grows.

Our new results are primarily in the area of higher-order generalized EGZ
constants, specifically $s_{2p}$ and $s_{3p}$.
In this paper, we prove the following theorems.

\begin{theorem}
    \label{thm:theorem1}
    The generalized EGZ constant $s_{2p}(C_p^r)$ is exponential in $r$.
    Specifically,
    \begin{equation*}
        s_{2p}(C_p^r) > \frac{p(1.25)^r}{2}.
    \end{equation*}
\end{theorem}
That is, there is a sequence of $p(1.25)^r/2$ elements in $C_p^r$ which
contains no zero-sum subsequence of length $2p$.
This is the first exponential lower bound on $s_{2p}$ for arbitrary dimensions.
In comparison, we can quite easily derive the exponential lower bound
\begin{equation*}
    s_p(C_p^r) > (p-1)2^r,
\end{equation*}
and do so in the \hyperref[sec:background]{following section}.

Another new result is an exact value for $s_{3p}$ and an improved lower bound
for $s_{2p}$ in the three-dimensional case.

\begin{theorem}
    \label{thm:theorem2}
    For the three-dimensional cyclic group $C_p^3$, we have
    \begin{enumerate}[i)]
        \item $5p + \frac{p-1}{2} + 3 \leq s_{2p}(C_p^3) \leq 6p - 3$
        \item $s_{3p}(C_p^3) = 6p - 3$.
    \end{enumerate}
\end{theorem}
The lower bound on $s_{2p}$ is due to Gao and Thangadurai (CITE THIS) - our
contribution is an improved upper bound.
The exact value of $s_{3p}$ is also new, and comes from careful examination
of the systems of congruences detailed in \autoref{sec:upperbounds}.
