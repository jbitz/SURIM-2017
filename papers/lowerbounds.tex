We now describe the proof that $s_{2p}(C_p^r)$ is exponential in $r$
The construction is probabilistic, and uses a somewhat crude union bound
to achieve its end.
While more advanced probabilistic techniques such as the L\'ovas Local Lemma
net small gains, they do not improve the exponential constant, which is the
most important element.

\begin{proof}[Proof of \autoref{thm:theorem1}]
    Let
    \begin{equation*}
        N = \frac{p(1.25 - \varepsilon)^r}{2}. 
    \end{equation*}
    Let $q = 1/5 + \varepsilon$.
    For the rest of the proof, we supress the $\varepsilon$, with the
    understanding that all quantities related to $N$ and $q$ are arbitrarily
    close to the stated values.
    Choose a sequence $X$ of $N$ random vectors in $\{0, 1\}^r$ as follows.
    For each $v \in X$, let $v_i = 1$ with probability $q$ and $v_i = 0$ with
    probability $1-q$, with each coordinate in each vector independent.
    Let $Z = (2p|X)$.
    We show that $\EE[Z] < 1$, and so there must be some possible $X$ for
    which $(2p|X) = 0$.

    Consider some arbitrary $2p$-subsequence $Y$ of $X$.
    For $Y$ to be zero-sum, each of the $r$ coordinates must sum to 0 mod $p$,
    and so contains exactly 0, $p$, or $2p$ ones.
    For any coordinate $i \leq r$, let $P_0$ be the probability that coordinate
    $i$ contains 0 ones, $P_p$ the probability that it contains $p$ ones, and
    $P_{2p}$ the probability that it contains $2p$ ones. 
    (Clearly, this is not dependent on $i$). We have
    \begin{equation*}
        \begin{split}
            P_0 &= (1-q)^{2p} \\
            P_p &= {2p \choose p}q^p(1-q)^p < 2^{2p} q^p(1-q)^p \\
            P_{2p} &= q^{2p},
        \end{split}
    \end{equation*}
    the values from a standard binomial distribution with probability of 
    success $q$.

    Now, if $Q$ is the probability that coordinate $i$ sums to zero, we have
    \begin{equation*}
        Q = P_0 + P_p + P_{2p} < (1-q)^{2p} + 2^{2p}q^p(1-q)^p + q^{2p}.
    \end{equation*}
    For $1/5 < q < 4/5$, we see that the second term dominates the first and third
    when $p$ is large.
    So, asymptotically, we have
    \begin{equation*}
        \begin{split}
            Q &< 2^{2p}\left(\frac{1}{5}\right)^p\left(\frac{4}{5}\right)^p \\
              &< \left(\frac{4}{5}\right)^{2p}.
        \end{split}
    \end{equation*}
    Since $Q$ is the probability that any one coordinate in $Y$ sums to zero,
    the probability that $Y$ as a whole is zero-sum is $Q^r$.
    Since each $2p$-subsequence of $X$ is zero-sum with equal probability, we
    have
    \begin{equation*}
        \begin{split}
            \EE[Y] &= {N \choose 2p}Q^r \\
                   &< \left(\frac{2N}{p}\right)^{2p}\left(\frac{4}{5}\right)^{2pr} \\
                   &< \left(\frac{5}{4}\right)^{2pr}\left(\frac{4}{5}\right)^{2pr} \\
                   &< 1,
        \end{split}
    \end{equation*}
    as desired.
\end{proof}

This result is notable for several reasons.
In addition to being the first exponential lower bound on $s_{2p}$ for general
$r$, it is also of the expected order of growth in $p$.
Indeed, an order of growth larger than linear in $p$ would be alarming, as it
would indicate that the linear formulas thus observed do not generalize
to higher dimensions.

As mentioned before, one might try several strategies for raising this lower bound.
In particular, taking $\EE[Y]$ is really just performing a union bound by summing
the probabilities of each event, without regard to intersection.
Another tactic one might try is the notion of \textit{alterations}.
If we relax our requirements and show that $\EE[Y] < N/2$, we can remove
one element from each offending (i.e. zero-sum) $2p$-subsequence, and be left
with a new set $X'$ whose size is on the same order as $X$, but with $(2p|X') = 0$.


