\documentclass{article}

\include{header}

\title{SURIM Report - Week 3}
\date{July 17, 2017}
\author{Jared Bitz}

\begin{document}
\maketitle

\section{Readings}

\begin{itemize}
    \item Reiher, Christian (2007), ``On Kemnitz' conjecture concerning
        lattice-points in the plane''
\end{itemize}

\section{An even better lower bound on $s_{2p}(C_p^r)$}

We show by a probabilistic argument the following bound:

\begin{prop}
    \begin{equation*}
        s_{2p}(C_p^r) > \frac{1.1^rp}{2}.
    \end{equation*}
\end{prop}

\begin{proof}
    Let $N = \left(\frac{p}{2}\right)1.1^r$. Choose a random sequence $S$ of
    $N$ vectors in $\{0, 1\}^r$ such that, for each $v \in S$, each
    coordinate of $v$ is (independently) $1$ with probability $q$ and $0$ with 
    probability $1-q$. 
    Here, we let $q = \frac{1}{4}$.
    Note that it is possible to have two or more of the same vector in $S$.
    Let $E$ be the event that $S$ has a zero-sum subsequence of length $2p$. 
    We will show that $P(E) < 1$, which implies there is some choice of $N$ 
    vectors in the Hamming cube with no such subsequence.

    Consider some subsequence $T \, | \, S$ of length $2p$.
    If the vectors in $T$ sum to 0, each coordinate must contain either $0$,
    $p$, or $2p$ ones.
    For a given coordinate $i \leq r$, the probability $P_1$ that it contains
    all zeroes is
    \begin{equation*}
        P_1 = (1-q)^{2p} = \left( \frac{3}{4} \right )^{2p}.
    \end{equation*}
    The probability that coordinate $i$ contains exactly $p$ ones is the standard
    binomial
    \begin{equation*}
        P_2 = {2p \choose p} q^p (1-q)^p < 2^{2p} (2^{-2})^p 
                \left( \frac{3}{4} \right )^p = \left( \frac{3}{4} \right )^p.
    \end{equation*}
    Lastly, the probability that coordinate $i$ contains exactly $2p$ ones is
    \begin{equation*}
        P_3 = q^{2p} = \left( \frac{1}{4} \right)^{2p}.
    \end{equation*}
    So, we see that coordinate $i$ sums to 0 mod $p$ with probability
    \begin{equation*}
        Q = P_1 + P_2 + P_3 < \left( \frac{3}{4} \right)^{2p}
            + \left( \frac{3}{4} \right)^{p} + \left( \frac{1}{4} \right)^{2p}.
    \end{equation*}
    As $p$ grows large, this sum is dominated by the 
    $\left( \frac{3}{4} \right)^{p}$ term, and so we can make the approximation
    $Q < \left( \frac{3}{4} \right)^{p}$.
    (To be slightly more rigorous, the below logic also works with
    $Q < \left( \frac{3}{4} \right)^{p} + \epsilon$, where we can make
    $\epsilon$ arbitarily small by making $p$ large. 
    Since $\epsilon$ shrinks faster than $1.1^p$, we're OK).

    Each coordinate is independent, so the probability that $T$ sums to the
    zero vector is $P_T = Q^r < \left( \frac{3}{4} \right)^{pr}$. 
    Since $S$ contains  ${N \choose 2p}$ subsequences of length $2p$, 
    the probability that one of them sums to $0$ is subject to the union
    bound
    \begin{equation*}
        \begin{split}
            P(E) = {N \choose 2p} P_T &< \left( \frac{2N}{p} \right)^{2p} 
            \left( \frac{3}{4} \right)^{pr} \\
            &< \left(\frac{1.1\sqrt{3}}{2}\right)^{2pr} \\
            &< 1.
        \end{split}
    \end{equation*}
    Therefore, there is some choice of $N$ elements in $\{0,1\}^r$ which contains
    no zero-sum subsequence of length $2p$, and $s_{2p}(C_p^r) > N$.
\end{proof}

This proof was mostly given by Jacob Fox.

\subsection{Improving the exponential constant}

There are several improvements we can make to the bound given above by refining
various aspects of the construction.
First, we seek to find an optimal value of $q$ that gives us as large of an
exponential constant as possible.
\begin{prop}
    The value of $q$ which yields the highest exponential constant in the
    construction above is $\frac{1}{5}$, yielding an constant of 1.25.
\end{prop}

\begin{proof}
    Let
    \begin{equation*}
        N = \frac{p(1+\delta)^r}{2}.
    \end{equation*}
    We wish to find the maximal $\delta$ for which this construction still holds.
    If the $P_2$ term in $Q$ still dominates, we need
    \begin{equation*}
        \begin{split}
            1 &> \left( \frac{2N}{p} \right)^{2p} [4q(1-q)]^{pr} \\
            1 &> [(1+\delta)^24q(1-q)]^{pr} \\
            \frac{1}{4} &> (1+\delta)^2q(1-q).
        \end{split}
    \end{equation*}
    So, $\delta$ can be largest when $q(1-q)$ is small.
    As above, the restrictions on $q$ are that the term $P_2$ dominates $P_1$
    and $P_3$. For $P_2$ to grow faster than $P_3$, we require
    \begin{equation*}
        \begin{split}
            2^{2p}q^p(1-q)^p &> q^{2p} \\
            [4q(1-q)]^p &> q^{2p} \\
            4q(1-q) &> q^2 \\
            \frac{4}{5} &> q.
        \end{split}
    \end{equation*}
    Likewise, to ensure $P_2$ dominates $P_1$, we get a symmetric bound 
    $q > \frac{1}{5}$.

    Since $q(1-q)$ approaches 0 at both ends of $[0, 1]$, our best choice is
    $q = \frac{1}{5}$ (or symmetrically, $q = \frac{4}{5}$).
    This gives us a maximal value of $\delta = .25$, so
    \begin{equation*}
        s_{2p}(C_p^r) > N = \frac{p(1.25)^r}{2}.
    \end{equation*}

\end{proof}

\subsection{Alterations}

Improvements to probabilistic constructions can often be made using the idea
of {\em alterations}. Let $X$ be the number of zero-sum subsequences
in our random collection of $N$ vectors. 
Then, if $\mathbb{E}[X] < \frac{N}{2}$,
we can just remove one element from each of the zero-sum subsequences, leaving
us with a set of size at least $\frac{N}{2}$ and no such subsequences. 
Our union bound above was essentially forcing $\mathbb{E}[X] < 1$, which is
more restrictive than necessary.
However, this insight does not lend us any improvements to the exponential 
constant $1 + \delta$.
For completeness, we record it anyway.

\begin{prop}
    \begin{equation*}
        s_{2p}(C_p^r) > \frac{p(1.25)^r}{2}.
    \end{equation*}
\end{prop}

\begin{proof}
    We begin by defining $N = \frac{p(1+\delta)^r}{2}$ as before, and seek
    the largest value of $\delta$ for which $\mathbb{E}[X] < \frac{N}{2}$.
    Again, let $Q$ be the probability that any coordinate in our randomly chosen
    $2p$-subsequence sums to zero. 
    With $q = \frac{1}{5}$ as determined in the previous section, the probablity
    that a given $2p$-subsequence is zero-sum is
    \begin{equation*}
        Q^r = [4q(1-q)]^{pr} = \left(\frac{4}{5}\right)^{2pr}.
    \end{equation*}
    Then, we have
    \begin{equation*}
        \begin{split}
            \mathbb{E}[X] = {N \choose 2p}Q^r &< 
            \left(\frac{2N}{p}\right)^{2p} \left(\frac{4}{5}\right)^{2pr} \\
            &< \left[\left(\frac{4}{5}\right)(1 + \delta)\right]^{2pr}.
        \end{split}
    \end{equation*}
    We require $\mathbb{E}[X] < \frac{N}{2}$, so we solve
    \begin{equation*}
        \begin{split}
            \left[\left(\frac{4}{5}\right)(1 + \delta)\right]^{2pr} 
                &< \frac{p(1+\delta)^r}{4} \\
            \left[\left(\frac{4}{5}\right)(1 + \delta)\right]^{p}  
                &< \left(\frac{\sqrt{p}}{2}\right)^{\frac{1}{r}}\sqrt{(1+\delta)} \\
            (1+\delta)^{\frac{2p-1}{2}} 
                &< \left(\frac{\sqrt{p}}{2}\right)^{\frac{1}{r}}
                    \left(\frac{5}{4}\right)^p \\
            \delta
                &< \left(\frac{\sqrt{p}}{2}\right)^{\frac{2}{r(2p-1)}}
                    \left(\frac{5}{4}\right)^{\frac{2p}{2p-1}} - 1.
        \end{split}
    \end{equation*}
    As $r$ and $p$ grow, the first term approaches 1.
    For $p$ large, the second term approaches $\frac{5}{4}$, so we again arrive
    at $\delta < \frac{1}{4}$, yielding the desired result.
\end{proof}

\subsection{The Local Lemma}

The following lemma is also useful in improving probabilistic constructions.
\begin{lemma}[Lov\'as Local Lemma]
    Let $A_1, \dots, A_n$ be events in any probability space, with $P(A_i) < p$
    for all $i$.
    Suppose that any $A_i$ is mutually independent with all but at most
    $d$ of the other $A_j$. If
    \begin{equation*}
        ep(d+1) \leq 1,
    \end{equation*}
    then 
    \begin{equation*}
        P\left(\bigcap_{i=1}^n \overline{A_i}\right) > 0.
    \end{equation*}
\end{lemma}
Essentially, it accounts for cases when the various $A_i$ are {\em mostly}
independent of each other.
The difficulty usually comes from calculating $d$.
If we choose $S$ to be a collection of $N$ elements, for each $2p$-subsequence
$T \, | \, S$, we define $A_T$ to be the event that $T$ is zero-sum.
Then, $A_T$ and $A_{T'}$ are independent if they have no elements in common.
There are ${N \choose 2p}$ possible $2p$-subsequences, and 
${N - 2p \choose 2p}$ of them are disjoint from a given $T$. So, we have
\begin{equation*}
    d = {N \choose 2p} - {N - 2p \choose 2p}.
\end{equation*}
We can minimize $P(A_i)$ just as before, so whether or not we get an improvement
is dependent on whether we can choose better bounds on $d$.
As it turns out, we really can't do much better than in the case without the
local lemma.

To see why this is the case, let $f(n) = {n \choose 2p}$.
Then, we can bound $f(n) - f(n - 2p)$ in terms of its (integral) derivatives:
\begin{equation*}
    2p(\Delta f)(n - 2p) \leq f(n) - f(n - 2p) \leq 2p(\Delta f)(n).
\end{equation*}
However, the integral derivative of $f$ is
\begin{equation*}
    (\Delta f)(n) = f(n) - f(n-1) = {n \choose 2p} - {n -1 \choose 2p} 
        = {n - 1 \choose 2p -1 }.
\end{equation*}
So, in total the best bound we have is
\begin{equation*}
    d = f(N) - f(N - 2p) \leq 2p {N - 1 \choose 2p - 1}.
\end{equation*}
Letting $N = (1+\delta)^r$, we end up again with the best case $\delta = .25$,
because multiplying $P(A_i)$ by $d$ is asymptotically the same as multiplying
it by ${N \choose 2p}$.
\end{document}
