\documentclass{article}

\include{header}

\title{SURIM Report - Week 7}
\date{August 13, 2017}
\author{Jared Bitz}

\begin{document}
\maketitle

\section{The Multi-Slice-Rank Method}

The concept of \textit{multi-slice-rank} was used by Eric Naslund to prove
an exponential upper bound of on $s_p(C_p^r)$.
Specifically, Naslund was able to show that
\begin{equation*}
    s_p(C_p^r) < (2p - 1)p!(J(p)p)^r,
\end{equation*}
where $J(p)$ is a real number between about $.918$ and $.841$, which decreases
monotonically as $p$ grows large.
We will first begin by outlining Naslund's argument, and then discuss the
improvements we attempted to make.

We first begin with some definitions pertaining to multi-slice-rank:
\begin{definition}
    Let $X_1, \dots, X_n$ be finite sets and $\FF$ a field.
    For any $0 < b < \frac{n}{2}$, a map
    $f: X_1 \times \dots \times X_n \rightarrow \FF$ is called a $b$-slice
    if it can be written as
    \begin{equation*}
        f(x_1, \dots, x_n) = g(\vec{x}_S)h(\vec{x}_{\{1, \dots, n\}-S})
    \end{equation*}
    for a set $S$ with $|S| = b$.
\end{definition}

\begin{definition}
    The multi-slice-rank of a function
    $f: X_1 \times \dots \times X_n \rightarrow \FF$ 
    is the minimum $m$ such that $f$ can be written as
    \begin{equation*}
        f = \sum_{i = 1}^m g_i
    \end{equation*}
    where the $g_i$ are all $b_i$ slices (and $b_i$ is not necessarily equal
    to $b_j$).
\end{definition}

Given the definition of slice rank, the core component of Naslund's proof is
the fact that diagonal tensors have a ``well-behaved'' multi-slice rank, in
that it is equal to the number of non-zero terms on the diagonal.
We state this formally as

\begin{lemma}
    Let $A \subset \FF$ for $\FF$ a field, and $f: A^k \rightarrow \FF$ be
    given by
    \begin{equation*}
        f(x_1, \dots, x_n) = \sum_{a \in A}c_a\delta_a(x_1)\dots \delta_a(x_n),
    \end{equation*}
    for $c_a \neq 0$. Then,
    \begin{equation*}
        \msr(f) = |A|.
    \end{equation*}
\end{lemma}

The idea is that, if we have a tensor which we know is diagonal, we can bound
the size of $A$ (i.e. the number of non-zero coefficients) by bounding the
multi-slice-rank of $f$.
The rest of the proof will involve defining a suitable tensor and approximating
its multi-slice-rank.

The tensor we choose will be given as follows. 
Let $F: A^p \rightarrow \FF_p$ be given by
\begin{equation}
    F(\vec{x}) = \prod_i^n\left[1 - \left(\sum_{j=1}^p x_{ji}\right)^{p-1}\right].
\end{equation}
This function evaluates to one if $\vec{x}$ is a zero-sum subsequence (in all
coordinates), and zero otherwise. The problem is that, since we've defined it
on $A^p$, any diagonal element $(x, \dots, x)$ forms a zero-sum subsequence, since
we're in characteristic $p$. 

To resolve this difficulty, we introduce another function that will let us detect
multiplicity in elements. 
For any $\sigma \in S_p$ (the symmetric group on $p$ elements), let
\begin{equation}
    f_\sigma(x_1, \dots, x_n) = 
    \begin{cases}
        1 & \sigma \text{\,\,fixes\,} \vec{x} \\
        0 & \text{\,\,otherwise} \\
    \end{cases}.
\end{equation}
Then, the following lemma can be verified:
\begin{lemma}
    The function
    \begin{equation*}
        \vec{x} \mapsto \sum_{\sigma \in S_p}f_\sigma(\vec{x})
    \end{equation*}
    evaluates to 1 if the elements of $\vec{x}$ are distinct, and zero otherwise. 
\end{lemma}

Using this lemma, we can construct our duplicate-correcting function $R_p$.
First, let $C_i$ be the set of all permutations in $S_p$ made up of exactly
$i$ disjoint cycles. Then, define
\begin{equation}
    R_p(\vec{x}) = \sum_{\sigma \in S_p - C_1,C_2}f_\sigma(\vec{x}).
\end{equation}
We then have the following lemma.
\begin{lemma}
    For $R_p$ as defined above, we have 
    \begin{equation*}
        R_p = 
        \begin{cases}
            1 & \text{if the $x_i$ are distinct} \\
            -1 & \text{if $x_1 = \dots = x_p$} \\
            \alpha(\vec{x}) & \text{if there are exactly two distinct $x_i$} \\
            0 & \textit{otherwise} \\
        \end{cases},
    \end{equation*}
    where $\alpha$ is some function that will become irrelevant later.
\end{lemma}

Now, the function whose multi-slice-rank we wish to bound is defined as
\begin{equation}
    I_p = R_pF_p.
\end{equation}

Another lemma of Naslund's asserts the following property.
\begin{lemma}
    Let $B \subset \FF_p$ be a subset such that no two points lie in the same
    1-dimensional subspace.
    Then, when restricted to $B^n$, we have
    \begin{equation*}
        I_p(\vec{x}) =
        \begin{cases}
            1 & \text{if the $x_i$ are distinct and sum to 0} \\
            -1 & \text{if $x_1 = \dots = x_p$} \\
            0 & \text{otherwise} \\
        \end{cases}.
    \end{equation*}
\end{lemma}

The above lemma follows fairly quickly from the values calculated for $R_p$ and
$F_p$.

Now, assume we have some $A \subset \FF_p^n$ such that $A$ contains no
zero-sum subsequence.
Then, $I_p|_{A^n}$ will be diagonal.
So, by Lemma 1, if we can bound the multi-slice-rank of $I_p$, we will have a
bound on the size of $A$. This leads us to the main theorem of the paper.

\begin{theorem}
    \begin{equation*}
        \msr(I_p) \leq 3(p-1)p!(J(p)p)^n,
    \end{equation*}
    where $J(p)$ is a real number betwen $.918$ and $.841$.
\end{theorem}

\begin{proof}
    We provide a sketch. 
    Consider some $A \subset \FF_p^n$ with no zero-sum-subsequence of length $p$.
    Along any 1-dimensional subspace of $\FF_p$, there can be at most $p-1$
    elements in $A$.
    (If there were more, then they would all sum to zero). 
    So, we choose $B \subset A$ such that no two points in $B$ lie on the same
    line through the origin, and
    \begin{equation*}
        |B| \geq \frac{|A|}{p-1}.
    \end{equation*}
    By Lemma 4, $I_p$ is diagonal on $B^p$, so we can use its multi-slice-rank
    to bound $|B|$, and thus $|A|$.
    We do so by considering each term in the sum making up $I_p$, which takes
    the form $f_\sigma F_p$. For a given $\sigma$, we can write it as
    \begin{equation*}
        \sigma = \pi_1 \dots \pi_t
    \end{equation*}
    for disjoing cycles $\pi_i$. Let $r_i$ be the number of elements in $\pi_i$.
    Then, $f_\sigma(\vec{x}) = 1$ if the elements of $\vec{x}$ are in
    multiplicities exactly agreeing with the $r_i$.
    Therefore, we can approximate $F_p$ on inputs of this form with the function
    \begin{equation*}
        M_{r_1, \dots, r_t}(z_1, \dots, z_t) = 
        \prod_{i=1}^n\left[1 - \left(\sum_{j=1}^t r_jz_{ji}\right)^{p-1}\right].
    \end{equation*}
    All we have done here is avoided extra terms by using the $r_j$ to count
    multiplcities, rather than summing up a bunch of identical elements.
    Then, we see that $f_\sigma F_p$ agrees with $f_\sigma M$ on all of $B$,
    since $f_\sigma$ is zero where $\sigma$ does not fix the input vector.
    
    It can be shown that $\msr(f_\sigma F_p) < \operatorname{slice-rank}(M_\sigma)$.
    So, we need only now to bound the slice-rank of $M_\sigma$.
    By continually slicing of low degree terms in the expanded version of the
    product making up $M_\sigma$, it can be shown that
    \begin{equation*}
        \operatorname{slice-rank}(M_{r_1, \dots, r_t}) \leq
        t \cdot \#\left\{v \in \{0, \dots, p-1\}^n \, | \, \sum v_i < 
        \frac{n(p-1)}{t}\right\}.
    \end{equation*}
    So, since multi-slice-rank is sub-additive, we have
    \begin{equation*}
        \begin{split}
            \msr(I_p) &\leq \sum_{t=3}^p \stirling{p}{t} 
            t \cdot \#\left\{v \in \{0, \dots, p-1\}^n \, | \, \sum v_i < 
            \frac{n(p-1)}{t}\right\} \\
            &\leq 3p!
            \cdot \#\left\{v \in \{0, \dots, p-1\}^n \, | \, \sum v_i < 
            \frac{n(p-1)}{3}\right\},
        \end{split}
    \end{equation*}
    since the size of the given set is maximized when $t = 3$. 
    (Note that the $\stirling{p}{t}$ terms are Stirling numbers of the first kind, 
    which count the size of $C_t$ in $S_p$).
    From here, a probabilistic argument using Chernoff bounds shows that the
    rightmost term is bounded by $(J(p)p)^n$, where the $J(p)$ term results from
    taking the infimum over a specifically chosen set of real numbers being raised
    to a power dependent on $n$ and $p$.
    This gives us in total that
    \begin{equation*}
        |B| \leq 3p!(J(p)p)^n,
    \end{equation*}
    as desired.
\end{proof}

One small detail to note is that Naslund's argument assumes that all elements
of $A$ are distinct, which is not necessarily the case in the EGZ problem.
Correcting for this is easy - instead of assuming that no $p-1$ elements of
$A$ lie in a 1-dimensional subspace, we instead must assume that no $2p-1$
have this property.
If they did, we could take them by isomorphism to $\FF_p$, where we know the
EGZ constant to be $2p-1$, giving us a zero-sum subsequence.
As such, the only change in the above result is that the $p-1$ factor that
comes from reducing $A$ to $B$ becomes a $2p-1$, which doesn't affect the
asymptotics.

\section{Reduction To Capsets}
In Naslund's slice-rank proof of bounds on the EGZ problem, the exponential
constant is ultimately derived from calculating the size of the set
\begin{equation*}
    S_t = \left\{ v \in \{0, \dots, 1\}^n \, : \,
    \sum v_i \leq \frac{n(p-1)}{t} \right\}.
\end{equation*}
Clearly, the maximum value of this expression (for fixed $n$ and $p$) occurs
at $t = 1$. 
By removing $C_1$ and $C_2$ from the sum comprising $R_p$ 
(c.f. the overview given above), Naslund is able to instead use $t = 3$ as
an upper bound. 
However, if we could also remove $C_3$ from the sum, we would be able to
get a large improvement in the exponential constant.
This type of removal does complicate the verification of diagonality for $I_p$
a good deal.

The way around this, though, is to make a choice of $B$ similar to the one that
let us disregard the $\alpha$ function mentioned above.
Now, instead of mandating that no two elements of $B$ lie in the same 1-dimensional
subspace, we must insist that no three elements lie in the same 2-dimensional
subspace.
(If this were the case, they would be linearly dependant, and a zero-sum subsequence
using multiplicities of the three would be possible).

So, the problem reduces to this: Given a large subset $A \subset \FF_p^n$, is
it possible to create a subset $B$ of size $\frac{|A|}{f(p)}$ with no three elements
summing to zero, where $f$ is polynomial in $p$? 
If this is the case, then we can instead calculate $|S_t|$ for $t = 4$, and get
a significant improvement on the exponential constant $J(p)p$.

The capset problem has really only been examined in detail for $\FF_3^n$, so any
improvements here for larger $p$ would be interesting.


\end{document}
