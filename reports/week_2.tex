\documentclass{article}

\include{header}

\title{SURIM Report - Week 2}
\date{July 10, 2017}
\author{Jared Bitz}

\begin{document}
\maketitle

\section{Readings}

\begin{itemize}
    \item Gao and Thangadurai: {\em On zero-sum subsequences of prescribed length}
\end{itemize}

\section{Better Lower Bounds on $s_{2n}(C_n^r)$}

Given the group $C_n^r$, begin by defining
\begin{equation*}
    T_a = \{ v \in \{0,1\}^r \, | \, \sum v_i = a \},
\end{equation*}
i.e. the set of all vectors in $\{0,1\}$ with exactly $a$ ones.
We would like to find an $a$ such that $T_a$ contains no zero-sum subsequence of
length $2n$.
Then, ${r \choose a}$ would provide a lower bound for $s_{2n}(C_n^r)$.
We will see that the value $a = c\log_2n$ works
as desired, for a suitable constant $c$.

For a purported $2n$-length zero-sum subsequence $S$ of $T_a$, suppose that
$l$ of the coordinates in the sum go up to $n$, $l'$ of them go up to $2n$,
and $r-l-l'$ remain at $0$.
If this is the case, then $S$ must be chosen from exactly ${l \choose a - l'}$
vectors - given $l'$ positions that must be 1, we can choose to fill an
additional $l$ using the remaining $a-l'$ 1's. However, the only case we need
to examine here is when $l = 2a$, which forces $l' = 0$.
(If we assume without loss of generality that the first $l'$ coordinates are
the ones in the $2n$ range, and the next $l$ are in the $n$ range, the case
$l = 2a$ contains all the others. 
In other words, choosing $l = 2a$ gives us the most freedom).
This gives us a total of ${2a \choose a}$ vectors. 

So, we have the bounds
\begin{equation}
    2^{2a} \geq {2a \choose a} \geq 2n.
\end{equation}
The right inequality asserts that we can choose at least $2n$ elements to
form our subsequence, and the left is a general bound on ${2n \choose n}$.
So, we will have a contradiction if
\begin{equation}
    \begin{split}
        2^{2a} &< 2n \\
        2a &< \log_2 2n \\
        a &< \frac{1}{2}\log_2 n.
    \end{split}
\end{equation}
Setting $a = c \log n$ (where $c$ is a constant less than $\frac{1}{2}$),
we obtain a lower bound 
\begin{equation}
    \begin{split}
        s_{2n}(C_n^r) &\geq |T_{c \log n}| = {r \choose c \log n} 
        \approx r^{c \log n},
    \end{split}
\end{equation}
for large $n$. Reformulating slightly, we see a nice symmetry
\begin{equation}
    s_{2n}(C_n^r) \geq 2^{c \log r \log n}.
\end{equation}

This is an improvement over the bounds discussed last week, which were linear
in both $r$ and $n$.

\section{A linear-algebraic approach to $s_{p}(C_p^3)$}

Suppose we are given a set vectors $\{v_1, \dots, v_m\}$ in $\FF_p^3$.
In the sequence given by taking $p-1$ of each $v_i$, is there a zero-sum
subsequence of length $p$? Moreover, how large must $m$ be to guarantee that
this is the case?
Since we have $p-1$ of each vector, we are allowed to choose any coefficient
in $\FF_p$ for each. So in general, the set of zero-sum subsequences
(regardless of length) is the set of solutions to the equation
\begin{equation*}
    a_1v_1 + \dots a_mv_m = 0.
\end{equation*}

Now, $\operatorname{span} \{v_i\}$ is a subspace of $\FF_p^3$.
The only interesting case is when this subspace has dimension 3
(i.e. it is all of $\FF_p^3$). Otherwise, we are in a situation isomorphic
to $\FF_p^2$ or $\FF_p$, for which we already know the EGZ constant.
In these low-dimensional cases, $m > \frac{2p-1}{p-1}$ and 
$\frac{4p-3}{p-1}$ respectively.

However, if the $v_i$ span all of our vector space, we can pick from them
three basis vectors, which we call $e_1, \dots e_3$. Rearrange so that these
are at the end of the list, and we are left with $v_1, \dots v_{m-3}$ vectors.
To express a zero-sum subsequence, write
\begin{equation}
    a_1e_1 + a_2e_2 + a_3e_3 + b_1v_1 + \dots b_{m-3}v_{m-3} = 0.
\end{equation}

Since the $e_i$ are a basis, we can write
\begin{equation}
    v_i = \sum_{j = 1}^3 c_j^{(i)}e_j.
\end{equation}
If we substitute (6) into (5), we see that
\begin{equation*}
    \left(a_1 + \sum_{i=1}^{m-3} b_ic_1^{(i)}\right)e_1 + \dots + 
    \left(a_r + \sum_{i=1}^{m-3} b_ic_3^{(i)}\right)e_3 = 0.
\end{equation*}
This is a dependence relation on basis vectors, so the coefficients
of each $e_i$ must be $0$ mod $p$. We can express this as the matrix equation
\begin{equation}
    \begin{pmatrix}
        1 & 0 & 0 & c_1^{(1)} &  \dots & c_1^{(m-3)} \\
        0 & 1 & 0 & \vdots    & \ddots& \vdots \\
        0 & 0 & 1 & c_3^{(1)} & \dots & c_3^{(m-3)}
    \end{pmatrix}
    \begin{pmatrix}
        a_1 \\ a_2 \\ a_3 \\b_1 \\ \vdots \\ b_{m-3}
    \end{pmatrix}
    = 0.
\end{equation}
If $A$ is the matrix above, $\ker A \subset \FF_p^m$ exactly describes the coefficients 
on zero-sum subsequences in the $\{v_i\}$. 
Also note that, because of the unit vectors in the leftmost columns of $A$, 
we have $\operatorname{rank} A = 3$. 
So, by the Rank-Nullity Theorem, $\dim \ker A = m - 3$.

Now, consider the subspace given by
\begin{equation*}
    U = \{ x \in \FF_p^m \, | \, \sum_{i=1}^m x_i \equiv 0 \mod p \}.
\end{equation*}
In other words, $U$ is the set of solutions to the equation
\begin{equation*}
    x_1 + x_2 + \dots + x_m = 0
\end{equation*}
over $\FF_p$. Clearly, $\dim U = m-1$. 
The subspace $U \cap (\ker A)$ then encodes all of the zero-sum subsequences
whose lengths are multiples of $p$.
Since $\dim U = m-1$, we see that
\begin{equation*}
    m - 4 \leq \dim (U \cap \ker A) \leq m - 3.
\end{equation*}

The hard part comes when trying to limit ourselves to vectors whose coordinates
sum to exactly $p$, and not some multiple of $p$.
If we could show that any one of these lies in $\ker A$, then we would be done.
As such, we have calculated that there are exactly 
${ p + m - 1 \choose m - 1} -m$ such vectors.
This is the number of ways to write $p$ as a sum of $m$ non-negative integers,
and then we subtract $m$ to discard solutions of the form $(0,0,\dots,0,p)$.
There are at worst $p^{m-1} - p^{m-4}$ elements in $U - \ker A$, so
we can naively try to pigeonhole in one of our desired vectors. This leads
to the inequality
\begin{equation}
    { p + m - 1 \choose m - 1} - m > p^{m-1} - p^{m-4},
\end{equation}
which unfortunately does not hold as $m$ increases.

This path being foiled, we attempted to count the number of one-dimensional
subspaces that could possibly fit in $U - \ker A$, and achieve a pigeonhole
argument in that direction. 
This number is $\frac{p^{m-1} - p^{m-4}}{p-1}$, but it is much harder to count
the number of one-dimensional subspaces generated by vectors whose coordinates
sum to exactly $p$. 
Sometimes, such vectors are the only examples in the subspace which they
generate. For example, if $p = m = 3$, then the vector $(1, 1, 1)$ has no
multiples whose coordinates sum to exactly $p$. However, if $p=7$ and $m = 2$,
then $2 \times (2,5) = (4, 3)$.
We were unable to successfuly count these subspaces, and even in the
overly optimistic world where {\em every} such vector generates a unique subspace,
there are likely not enough to achieve the desired pigeonhole bound.

Overall, this translation of the problem yielded no new results, but is appreciated
for bringing slightly more geometric insight to its structure.
It is possible that there are more advanced algebraic techniques to apply,
but this one goes on the back-burner for now.
%--------------------------------------------------------------------------
\section{Further Inquiries}
In the Friday meeting, we drew up a list of things to investigate for the
next week.
\begin{itemize}
    \item If we choose vectors consisting of only 0's, 1's, and 2's, can
        we get better lower bounds on $s_kp(C_p^r)$ than staying withing
        the Hamming cube? What is the optimal construction?
    \item Can we write a program to perform an intelligently guided search
        through constructions to improve these lower bounds?
    \item Is it advantageous to choose vectors in the Hamming cube with
        a certain distance between them? If so, what is the optimal distance?
    \item Can we generalize and Cauchy-Davenport theorem to $s_kp(C_p^r)$,
        especially when we are given subsequences with many distinct elements?
\end{itemize}
\end{document}
